package com.example.esp32_udemy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.Electronica117_MQTT.electronica117_mqtt.Interfaces.MQTTCallbackListener;
import com.Electronica117_MQTT.electronica117_mqtt.Interfaces.MQTTListener;
import com.Electronica117_MQTT.electronica117_mqtt.MQTT;
import com.electroncia117.electronica117_ui.Grafica;
import com.electroncia117.electronica117_ui.IndicadorCircular;
import com.electroncia117.electronica117_ui.LED;
import com.electroncia117.electronica117_ui.Termometro;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    /** MQTT **/
    private String Topico = "Udemy/ESP32/Curso/habitacion1";

    /** Vistas **/
    private Grafica graficaTemperatura;
    private Grafica graficaHumedad;
    private Termometro termometroTemperatura;
    private IndicadorCircular indicadorCircularHumedad;
    private LED led;
    private SwitchCompat LedSw;

    /** Objetos **/
    private Medidas medidas;
    private RGB rgb;
    private DHT11 dht11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        graficaTemperatura = findViewById(R.id.GraficaTemperatura);
        graficaHumedad = findViewById(R.id.GraficaHumdeda);
        termometroTemperatura = findViewById(R.id.termometroTemperatura);
        indicadorCircularHumedad = findViewById(R.id.IndicadorCircularHumedad);
        led = findViewById(R.id.LED);
        LedSw = findViewById(R.id.LedSw);

        /** Configurar vistas **/
        graficaTemperatura.setMaxAmplitude(50);
        graficaTemperatura.setText("°C");

        termometroTemperatura.setMax(50);
        termometroTemperatura.setUnits("°C");
        termometroTemperatura.gradientEnabled(true);

        graficaHumedad.setMaxAmplitude(100);
        graficaHumedad.setText("%");
        graficaHumedad.setColorGradient_1(getColor(R.color.Gradiente1));
        graficaHumedad.setColorGradient_2(getColor(R.color.Gradiente2));

        indicadorCircularHumedad.setMax(100);
        indicadorCircularHumedad.setUnits("%");
        indicadorCircularHumedad.gradientEnabled(true);
        indicadorCircularHumedad.setColorGradient_1(getColor(R.color.Gradiente1));
        indicadorCircularHumedad.setColorGradient_2(getColor(R.color.Gradiente2));

        /** Set el listener al switch **/
        LedSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MQTT.Publish("Udemy/ESP32/Curso/habitacion1/Led", String.valueOf(isChecked), new MQTTListener() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onFailure() {

                    }
                });
            }
        });

    }




    @Override
    protected void onStart() {
        super.onStart();
        /**
         * Si quieres cambiar de server utiliza
         * MQTT.setServer(server);
         *
         * Para cambiar el puerto utiliza
         * MQTT.setPuerto(puerto);
         **/

        /** Conexión a server MQTT **/
        MQTT.ConnectMQTT(new MQTTListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(), "Conectado!!!", Toast.LENGTH_SHORT).show();
                /** Suscripción a borker  **/
                MQTT.Suscribe(Topico, new MQTTListener() {
                    @Override
                    public void onSuccess() {
                        /** Seteamos el Callback **/
                        MQTT.Callback(new MQTTCallbackListener() {
                            @Override
                            public void connectionLost(String cause) {

                            }

                            @Override
                            public void messageArrived(String topic, String message) {
                                Gson gson = new Gson();
                                /** Convertimos un String en formato Json a un objeto medidas **/
                                medidas = gson.fromJson(message, Medidas.class);

                                /** Solicitamos todos los datos a nuestro objeto **/
                                rgb = medidas.getRgb();
                                led.setRGB(rgb.getR(), rgb.getG(), rgb.getB());

                                dht11 = medidas.getDht11();
                                termometroTemperatura.setTemperature(dht11.getTemperatura());
                                graficaTemperatura.setValue(dht11.getTemperatura());

                                indicadorCircularHumedad.setValue(dht11.getHumedad());
                                graficaHumedad.setValue(dht11.getHumedad());
                            }
                        });
                    }

                    @Override
                    public void onFailure() {

                    }
                });
            }

            @Override
            public void onFailure() {
                Toast.makeText(getApplicationContext(), "No se conecto :(", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        MQTT.Disconect(new MQTTListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure() {

            }
        });
    }

}