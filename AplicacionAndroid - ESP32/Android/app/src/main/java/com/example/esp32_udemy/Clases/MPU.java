package com.example.esp32_udemy.Clases;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MPU {

    private SensorMPU Ac = new SensorMPU();
    private SensorMPU Gi = new SensorMPU();

    public MPU() {
    }

    public MPU(SensorMPU ac, SensorMPU gi) {
        Ac = ac;
        Gi = gi;
    }

    public SensorMPU getAc() {
        return Ac;
    }

    public void setAc(SensorMPU ac) {
        Ac = ac;
    }

    public SensorMPU getGi() {
        return Gi;
    }

    public void setGi(SensorMPU gi) {
        Gi = gi;
    }

}
