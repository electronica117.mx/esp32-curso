package com.example.esp32_udemy.Clases;


public class SensorMPU {
    private int x = 1;
    private int y = 2;
    private int z = 3;

    public SensorMPU() {
    }

    public SensorMPU(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
