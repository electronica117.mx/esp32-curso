package com.example.esp32_udemy.Clases;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Medidas2 {
    private int humedad = 20;
    private int temperatura = 27;
    private String msn = "Hola mundo!!!";
    private MPU mpu = new MPU();

    public Medidas2() {
    }

    public Medidas2(int humedad, int temperatura, String msn, MPU mpu) {
        this.humedad = humedad;
        this.temperatura = temperatura;
        this.msn = msn;
        this.mpu = mpu;
    }

    public int getHumedad() {
        return humedad;
    }

    public void setHumedad(int humedad) {
        this.humedad = humedad;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public MPU getMpu() {
        return mpu;
    }

    public void setMpu(MPU mpu) {
        this.mpu = mpu;
    }

}
