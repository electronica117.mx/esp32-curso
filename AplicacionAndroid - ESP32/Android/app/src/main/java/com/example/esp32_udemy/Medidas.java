package com.example.esp32_udemy;

public class Medidas {

    private RGB RGB;
    private DHT11 DHT11;

    public RGB getRgb() {
        return RGB;
    }

    public void setRgb(RGB rgb) {
        this.RGB = rgb;
    }

    public DHT11 getDht11() {
        return DHT11;
    }

    public void setDht11(DHT11 dht11) {
        this.DHT11 = dht11;
    }

    public Medidas(RGB rgb, DHT11 dht11) {
        this.RGB = rgb;
        this.DHT11 = dht11;
    }

    public Medidas() {
    }
}
