#include <Arduino.h>
#include <WiFi.h>
#include <FirebaseESP32.h>
#include <PubSubClient.h>
#include <SimpleDHT.h>

/* Red WiFi */
#define SSID "Enterprise"
#define PASS "0112358Chino"
void InitWiFi();

/* Firebase */
#define DB_URL "esp32-a80a1.firebaseio.com"                     // URL de la base de datos.
#define SECRET_KEY "TJplaGSuc2dTHyKHJWxosgrzDrFs4z29oWgXbWMN"   // Secreto de la base de datos.
FirebaseData myFirebaseData;                                    // Objeto de tipo FirebaseData, ayuda a leer y escribri en la base de datos.
String myJsonStr;                                     
FirebaseJson Medidas, DHT11, RGB;
unsigned long TiempoFirebase = 0;
#define DelayFirebase 100

/* MQTT */
const char* MQTT_Server = {"broker.hivemq.com"};
int Puerto = 1883;
String topico = "Udemy/ESP32/Curso/habitacion1";
WiFiClient wifiClient;
PubSubClient MQTTClient(wifiClient);
String ClientId;
#define IdName "Edgar-"
void ConnectMQTT();
void callback(char *topico, uint8_t *msg, unsigned int longitud);
String callbackMsn;
unsigned long TiempoMQTT = 0;
#define DelayMQTT 150

/* Configuracion de terminales para led RGB*/
#define R_ADC 32
#define G_ADC 33
#define B_ADC 34

/* Configuracion de terminales para DHT11 */
#define PinDHT 4
float Temperatura = 0, Humedad = 0;
SimpleDHT11 DHT(PinDHT);
unsigned long TiempoDHT = 0;
#define SampleDHT 1200

/** Pin del LED **/
#define PinLed 2

void setup() {
  Serial.begin(9600);
                                             
  InitWiFi();                                                     // Funcion para conectarnos a WiFi (Esta hasta el final)

  Firebase.begin(DB_URL, SECRET_KEY);                             // Inicializamos Firebase, dandole como argumento el URL y secreto de la base de datos.
  Firebase.reconnectWiFi(true);                                   // Reconexion 

  MQTTClient.setServer(MQTT_Server, Puerto);
  MQTTClient.setCallback(callback);

  pinMode(PinLed, OUTPUT);
  digitalWrite(PinLed, LOW);                 

  analogReadResolution(9);
}

void loop() {
  /* Conexion a MQTT */
  ConnectMQTT();

  /* Lectura analogica RGB */
  RGB.set("R", analogRead(R_ADC)/2);
  while(adcBusy(R_ADC));
  RGB.set("G", analogRead(G_ADC)/2);
  while(adcBusy(G_ADC));
  RGB.set("B", analogRead(B_ADC)/2);
  while(adcBusy(B_ADC));

  /* Lectura del DHT11 cada 1.2s */
  if(millis() > TiempoDHT + SampleDHT){
    if(DHT.read2(&Temperatura, &Humedad, NULL) == SimpleDHTErrSuccess){
      DHT11.set("Temperatura", Temperatura);
      DHT11.set("Humedad", Humedad);
      //Serial.println("Temperatura: "); Serial.println(Temperatura);
      //Serial.println("Humedad: "); Serial.println(Humedad);
      TiempoDHT = millis();
    }else{
      Serial.println("Error DHT11");
    }
  }

  Medidas.set("RGB", RGB);
  Medidas.set("DHT11", DHT11);

  Medidas.toString(myJsonStr);
 
  /* Publicar myJsonStr */
  if(millis() > TiempoMQTT + DelayMQTT){
    MQTTClient.publish(topico.c_str(), myJsonStr.c_str());
    TiempoMQTT = millis();
  }

  /* Subir a la base de datos cada segundo los valores del DHT11 */
  if(millis() > TiempoFirebase + DelayFirebase){
    Firebase.set(myFirebaseData, "Medidas", Medidas);
    TiempoFirebase = millis();
  }
 
}



void InitWiFi(){
  WiFi.begin(SSID, PASS);                   // Inicializamos el WiFi con nuestras credenciales.
  Serial.print("Conectando a ");
  Serial.print(SSID);

  while(WiFi.status() != WL_CONNECTED){  
    Serial.print(".");
    delay(50);
  }

  if(WiFi.status() == WL_CONNECTED){      // Si el estado del WiFi es conectado entra al If
    Serial.println("");
    Serial.println("");
    Serial.println("Conxion exitosa!!!");
  }

  Serial.println("");
  Serial.print("Tu IP es: ");
  Serial.println(WiFi.localIP());
}


void ConnectMQTT(){
  if(!MQTTClient.connected()){
    ClientId = IdName;
    ClientId += String(random(1000));
    Serial.println(ClientId);
    if(MQTTClient.connect(ClientId.c_str())){
          Serial.println("Conexion exitosa a broker");
          // Suscripciones
          if(MQTTClient.subscribe("Udemy/ESP32/Curso/habitacion1/Led")){
            Serial.println("Suscrito");
          }
      }else{
          Serial.println("Algo paso :(");
    }
  }
MQTTClient.loop();
}


void callback(char *topico, uint8_t *msg, unsigned int longitud){
  for(int j=0; j<longitud; j++){
      callbackMsn += (char)msg[j];
    }

    if(callbackMsn.equals("true")){
      digitalWrite(PinLed, HIGH);
    }else{
      digitalWrite(PinLed, LOW);
    }
 
  callbackMsn = "";
}